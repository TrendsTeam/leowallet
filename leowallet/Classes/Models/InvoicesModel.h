//
//  InvoicesModel.h
//  leowallet
//
//  Created by admin on 28.08.2018.
//

#import <Foundation/Foundation.h>

@interface InvoicesModel : NSObject
@property (strong, nonatomic) NSNumber *id;
@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *account;
@end
