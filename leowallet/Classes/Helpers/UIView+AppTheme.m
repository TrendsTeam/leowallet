//
//  UIView+AppTheme.m
//  leowallet
//
//  Created by admin on 28.08.2018.
//

#import "UIView+AppTheme.h"

@implementation UIView (leowallet)

 - (void)createHeadGradient{
    UIColor *leftColor = [UIColor colorWithRed:251.0/255.0 green:5.0/255.0 blue:76.0/255.0 alpha:1.0];
    UIColor *rightColor = [UIColor colorWithRed:56.0/255.0 green:55.0/255.0 blue:216.0/255.0 alpha:1.0];
    
     [self create2ColorsGradientWithColorLeft:leftColor
                                   rightColor:rightColor
                                   startPoint:CGPointMake(1.0, 0.8)
                                     endPoint:CGPointMake(0.0, 0.2)];
}

- (void)createBottomGradient{
    UIColor *leftColor = [UIColor colorWithRed:59.0/255.0 green:59.0/255.0 blue:72.0/255.0 alpha:1.0];
    UIColor *rightColor = [UIColor colorWithRed:38.0/255.0 green:38.0/255.0 blue:48.0/255.0 alpha:1.0];
    
    [self create2ColorsGradientWithColorLeft:leftColor
                                  rightColor:rightColor
                                  startPoint:CGPointMake(0.0, 0.5)
                                    endPoint:CGPointMake(1.0, 0.5)];
}

- (void)create2ColorsGradientWithColorLeft:(UIColor*)leftColor rightColor:(UIColor*)rightColor startPoint:(CGPoint)startPoint endPoint:(CGPoint)endPoint{
    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
    theViewGradient.colors = [NSArray arrayWithObjects: (id)leftColor.CGColor, (id)rightColor.CGColor, nil];
    theViewGradient.frame = self.bounds;
    
    theViewGradient.startPoint = startPoint;
    theViewGradient.endPoint = endPoint;
    
    [self.layer insertSublayer:theViewGradient atIndex:0];
}

@end
