//
//  DataManager.h
//  leowallet
//
//  Created by admin on 28.08.2018.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject

+ (DataManager *)sharedInstance;
- (NSDictionary*)getJSONDatafromFile;
- (NSArray*)getAccounts:(id)source;
- (NSArray*)getInvoices:(id)source;

@end
