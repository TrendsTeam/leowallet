//
//  UIView+AppTheme.h
//  leowallet
//
//  Created by admin on 28.08.2018.
//

#import <UIKit/UIKit.h>

@interface UIView (leowallet)

- (void)createHeadGradient;
- (void)createBottomGradient;

@end
