//
//  Constants.h
//  leowallet
//
//  Created by admin on 28.08.2018.
//

#ifndef IBAConstants_h
#define IBAConstants_h

#pragma mark - Types -
typedef NS_ENUM(NSInteger, SectionType) {
    InvoicesSection = 0,
    FavoriteSection,
    LastSection
};

#pragma mark - Parameters: -
static NSString * const AccountKey = @"account";
static NSString * const BalanceKey = @"balance";
static NSString * const InvoicesKey = @"invoices";
static NSString * const FavoritesKey = @"favorites";
static NSString * const LastKey = @"last";
static NSString * const FavoritesSelectionKey = @"Избранное";
static NSString * const LastSelectionKey = @"Избранное";

static NSString * const SectionHeaderIdentifierKey = @"SectionHeader";

static NSString * const SeparatorKey = @"\u00a0";

static int const kSectionsCount = 3;
static int const kHeightForHeader = 32;
#endif /* IBAConstants_h */
