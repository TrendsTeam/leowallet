//
//  DataManager.m
//  leowallet
//
//  Created by admin on 28.08.2018.
//

#import "DataManager.h"

#import "InvoicesModel.h"
#import "AccountModel.h"

@implementation DataManager

+ (DataManager *)sharedInstance {
    static dispatch_once_t pred;
    static DataManager *shared = nil;
    dispatch_once(&pred, ^{
        shared = [[DataManager alloc] init];
    });
    return shared;
}


- (NSDictionary*)getJSONDatafromFile{
    
    NSString *filepath=[[NSBundle mainBundle] pathForResource:@"dashboard" ofType:@"json"];
    NSData *returnedData = [NSData dataWithContentsOfFile:filepath];
    id object;
    if(NSClassFromString(@"NSJSONSerialization")){
        NSError *error = nil;
        object = [NSJSONSerialization
                     JSONObjectWithData:returnedData
                     options:0
                     error:&error];
    }
    return  object;
    
}
- (NSArray*)checkForArray:(id)source{
    NSArray *dataArr;
    if([source isKindOfClass:[NSArray class]]){
        dataArr = source;
    }
    else{
        dataArr = [NSArray arrayWithObject:source];
    }
    return dataArr;
    
}

- (NSArray*)getAccounts:(id)source{
    NSArray *dataArr = [self checkForArray:source];
    NSMutableArray * accountArr= [[NSMutableArray alloc] init];
    for (NSDictionary *accountDic in dataArr) {
        AccountModel *account = [[AccountModel alloc] init];
        
        for (NSString *key in accountDic) {
            if ([account respondsToSelector:NSSelectorFromString(key)]) {
                [account setValue:[accountDic valueForKey:key] forKey:key];
                
            }
        }
        [accountArr addObject:account];
    }
    return accountArr;
}

- (NSArray*)getInvoices:(id)source{
    NSArray *dataArr = [self checkForArray:source];
    NSMutableArray * invoiceArr= [[NSMutableArray alloc] init];
    for (NSDictionary *invoiceDic in dataArr) {
        InvoicesModel *invoice = [[InvoicesModel alloc] init];
        
        for (NSString *key in invoiceDic) {
            if ([invoice respondsToSelector:NSSelectorFromString(key)]) {
                [invoice setValue:[invoiceDic valueForKey:key] forKey:key];
            }
        }
        [invoiceArr addObject:invoice];
        
    }
    return invoiceArr;
}
@end
