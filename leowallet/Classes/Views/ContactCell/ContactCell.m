//
//  ContactCell.m
//  leowallet
//
//  Created by admin on 28.08.2018.
//

#import "ContactCell.h"
#import "Constants.h"

#import "InvoicesModel.h"
#import "AccountModel.h"

@interface ContactCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactLabel;
@property (weak, nonatomic) IBOutlet UIImageView *infoIcon;
@property (weak, nonatomic) IBOutlet UIImageView *starIcon;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint* constraint;

@end

@implementation ContactCell

+ (NSString *)cellIdentifier {
    return @"ContactCell";
}

- (void)configureCellWithModel:(id)model{
    if([model isKindOfClass:[AccountModel class]]){
        AccountModel* m = model;
        self.nameLabel.text = m.name;
        if([m.name length]){
            self.contactLabel.text = m.account1;
        }
        else{
            self.nameLabel.text = m.account1;
            self.contactLabel.text = @"";
            
        }
        if(self.tag == FavoriteSection){
            self.starIcon.image = [UIImage imageNamed:@"StarActive"];
        }
        if(![self.contactLabel.text length]){
                    [self.constraint setActive:NO];
        }
    }
    else if([model isKindOfClass:[InvoicesModel class]]){
        InvoicesModel* m = model;
        self.nameLabel.text = m.name;
        self.contactLabel.text = m.account;
    }
    
}

@end
