//
//  SectionHeader.h
//  leowallet
//
//  Created by admin on 28.08.2018.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface SectionHeader : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *HeaderLabel;


@end
