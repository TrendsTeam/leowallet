//
//  ContactCell.h
//  leowallet
//
//  Created by admin on 28.08.2018.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell

+ (NSString *)cellIdentifier;
- (void)configureCellWithModel:(id)model;
@end
