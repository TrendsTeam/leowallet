//
//  ViewController.m
//  leowallet
//
//  Created by admin on 27.08.2018.
//

#import "ViewController.h"
#import "DataManager.h"

#import "Constants.h"
#import "UIView+AppTheme.h"

#import "ContactCell.h"
#import "SectionHeader.h"


@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIView *gradientView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;

@property (weak, nonatomic) IBOutlet UILabel *walletLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceWholePartLabel;
@property (weak, nonatomic) IBOutlet UILabel *balancePennyPartLabel;

@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *invoices;
@property (strong, nonatomic) NSArray *favoriteAccounts;
@property (strong, nonatomic) NSArray *lastAccounts;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.gradientView createHeadGradient];
    [self.bottomView createBottomGradient];
    [self getData];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
}

#pragma mark - Actions -

- (IBAction)buttonPressed:(UIButton*)sender
{
    [self showGagWithTitle:sender.titleLabel.text];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [searchBar resignFirstResponder];
}

-(void)dismissKeyboard {
    [self.searchBar resignFirstResponder];
}


#pragma mark - DataManager -

- (void)getData{

    NSDictionary* data = [[DataManager sharedInstance] getJSONDatafromFile];
    [self.walletLabel setText:[data objectForKey:AccountKey]];
    [self configureBalanceLabel:[data objectForKey:BalanceKey]];
    self.invoices =  [[DataManager sharedInstance]getInvoices:[data objectForKey:InvoicesKey]];
    self.favoriteAccounts =  [[DataManager sharedInstance]getAccounts:[data objectForKey:FavoritesKey]];
    self.lastAccounts =  [[DataManager sharedInstance]getAccounts:[data objectForKey:LastKey]];
    [self.tableView reloadData];
    
}

#pragma mark - custom methods -

- (NSArray*)getArrayFromSectionId:(NSInteger)section {
    switch (section) {
        case InvoicesSection:
            return self.invoices;
            break;
        case FavoriteSection:
            return self.favoriteAccounts;
            break;
        case LastSection:
            return self.lastAccounts;
            break;
        default:
            return nil;
            break;
    }
}

- (void)configureBalanceLabel:(NSNumber*)value{
    double d = value.doubleValue/100,x,y;
    y = modf(d, &x);
    
    [self.balancePennyPartLabel setText:[NSString stringWithFormat:@"%i",(int)(y*100)]];
    
    NSNumberFormatter * formatter = [NSNumberFormatter new];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    [formatter setGroupingSeparator:SeparatorKey];
    [self.balanceWholePartLabel setText:[formatter stringFromNumber:[NSNumber numberWithInteger:(int)x]]];
    
}

- (void)showGagWithTitle:(NSString*)title{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:title
                          message:nil
                          delegate:self
                          cancelButtonTitle:@"Ok"
                          otherButtonTitles:nil];
    [alert show];
}

@end
