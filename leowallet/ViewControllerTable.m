//
//  ViewControllerTable.m
//  leowallet
//
//  Created by admin on 29.08.2018.
//

#import "ViewControllerTable.h"
#import "Constants.h"


#import "ContactCell.h"
#import "SectionHeader.h"

@implementation ViewController (table)

#pragma mark - UITableView DataSource -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return kSectionsCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[self getArrayFromSectionId:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    ContactCell *cCell = [tableView dequeueReusableCellWithIdentifier:[ContactCell cellIdentifier] forIndexPath:indexPath];
    NSArray* data = [self getArrayFromSectionId:indexPath.section];
    cCell.tag = indexPath.section == FavoriteSection?FavoriteSection:0;
    [cCell configureCellWithModel:data[indexPath.row]];
    return cCell;
}

#pragma mark - UITableView Delegate -

- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == InvoicesSection) {
        return nil;
    }
    SectionHeader* header = [tableView dequeueReusableCellWithIdentifier:SectionHeaderIdentifierKey];
    switch (section) {
        case FavoriteSection:
            header.HeaderLabel.text = FavoritesSelectionKey;
            break;
        case LastSection:
            header.HeaderLabel.text = LastSelectionKey;
            break;
            
        default:
            break;
    }
    
    return header;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == InvoicesSection){
        return CGFLOAT_MIN;
    }
    else{
        return kHeightForHeader;
    }
}

@end
